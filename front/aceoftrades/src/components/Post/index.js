// src/components/Post/index.js
import React, { Component } from "react";
import { Container, Row, Col, Image, Card, Button } from 'react-bootstrap';


import "./Post.css";

class Post extends Component {
  render() {
    const nickname = this.props.nickname;
    const image = this.props.image;
    const caption = this.props.caption;
    const userId = this.props.userId;
    const url = "http://localhost:3000/user/"+userId;
    const postTitle = this.props.postTitle;
    const price = this.props.price;
    console.log(image);
    return (
        <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={image} />
        <Card.Body>
          <Card.Title>{postTitle}</Card.Title>
          <Card.Text>
            {caption}
          </Card.Text>
          <Card.Text>
            ${price}
          </Card.Text>
          <Card.Link href={url}>{nickname}</Card.Link>
        </Card.Body>
      </Card>
    );
  }
}
export default Post;