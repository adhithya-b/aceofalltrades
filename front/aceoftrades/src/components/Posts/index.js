// src/components/Posts/index.js
import React from "react";
import Post from "../Post";
import {Container, CardColumns} from 'react-bootstrap';
import axios from 'axios';



class Posts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            posts: []
        };

        //this.handleClick = this.handleClick.bind(this);
        //this.renderRow = this.renderRow.bind(this);
        this.loadPosts = this.loadPosts.bind(this);
    }

    componentDidMount() {
          // fetch the initial posts
          this.loadPosts();
        }

    renderPost(post, index) {
        const p = post;
        console.log(p.pfp)
        return (
            <Post 
                            nickname={p.user.name} 
                            image={p.user.pfp} 
                            caption={p.description}
                            userId= {p.user_id}
                            postTitle= {p.title}
                            price={p.price}
                            key={p._id}/>
        );

    }

    async loadPosts() {
        var self = this;
        const headers = { 'Access-Control-Allow-Origin': '*' };
        let currentPosts = [];
        let api = "http://10.0.33.99:5000/api/posts";
        await axios.get(api, headers).then(function (response) {
            var objects = (response.data)
            console.log(response.data);
            for (let i = 0; i < objects.length; ++i) {
                const post = objects[i];
                currentPosts.push(post);
            }
            self.setState({
                posts: currentPosts
            });
        });
    }
    render() {
        document.body.style.backgroundImage = "none";
        return (
            <div>
                {this.state.posts.length == 0 ? 
                    <h2>Loading posts...</h2> : 
                    (
                    <div>
                        <Container>
                        <CardColumns>
                        {this.state.posts.map(this.renderPost)}
                        )
                        </CardColumns>

                        </Container>
                    </div>)
                }
            </div>
        );
    }

}


export default Posts;
