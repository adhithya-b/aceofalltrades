// src/components/Header/index.js
import React from "react";
import { BrowserRouter as Router, Link } from 'react-router-dom';
import "../../App.css";
import { Nav, Navbar, Button, FormControl, Form } from "react-bootstrap";



class Header extends React.Component{
    render(){
        return (
        <div className="AppCon">
          <Navbar fluid collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top" justify-content-between>
            <Navbar.Brand href="/">
              Ace of Trades
              </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Item>
                  <Nav.Link eventKey="1" as={Link} to="/posts">Posts</Nav.Link>
                </Nav.Item>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
       );
    }   
}
export default Header;