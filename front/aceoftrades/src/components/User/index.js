import React from 'react';
import { Image, CardColumns } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import DefaultPic from '../../assets/Default.png';
import axios from 'axios';
import Post from '../Post';



class UserModel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},            
            posts: []
        };

        this.renderUser = this.renderUser.bind(this);
        this.renderPosts = this.renderPosts.bind(this);
    }

    componentDidMount() {
        console.log("component did mount called");
        // the congressid must be passed in otherwise this will not work
        const headers = { 'Access-Control-Allow-Origin': '*' };
        const myURL = window.location.href;
        const num = myURL[myURL.length - 1];

        let idStr = num;
        const api = 'http://10.0.33.99:5000/api/user/' + idStr;
        console.log("I am in the else clause ");
        axios.get(api, headers).then((response) => {

            const myUser = (response.data)
            console.log(myUser.pic)
            if (myUser.pic === 'undefined') {
                myUser.pic = DefaultPic;
            }

            this.setState({ user: myUser, 
            });
            console.log("axios request done");
        });
        this.loadPosts(num);
    }

    async loadPosts(filter) {
        var self = this;
        const headers = { 'Access-Control-Allow-Origin': '*' };
        let currentPosts = [];
        let api = "http://10.0.33.99:5000/api/posts";
        await axios.get(api, headers).then(function (response) {
            var objects = (response.data)
            for (let i = 0; i < objects.length; ++i) {
                const post = objects[i];
                console.log(filter);
                if(post.user_id == filter){
                    currentPosts.push(post);
                }
            }
            self.setState({
                posts: currentPosts
            });
        });
    }
    renderPosts() {
        document.body.style.backgroundImage = "none";
        return (
            <div>
                {this.state.posts.length == 0 ? 
                    <h2>Loading posts...</h2> : 
                    (
                    <div>
                        <Container>
                        <CardColumns>
                        {this.state.posts.map(this.renderPost)}
                        </CardColumns>
                        </Container>
                    </div>)
                }
            </div>
        );
    }

    renderPost(post, index) {
        const p = post;
        console.log(p.pfp)
        return (
            <Post 
                            nickname={p.user.name} 
                            image={p.pictures[0]} 
                            caption={p.description}
                            userId= {p.user_id}
                            postTitle= {p.title}
                            price={p.price}
                            key={p._id}/>
        );

    }


    renderUser() {
        // under the assumption that this.state.congressman has been loaded in already
        return (
                <section className="section">
                    <div className="container">
                        <header className="section-header">
                            <div className="text-center">
                                <h1>{this.state.user.name}</h1>
                                <Image src={this.state.user.pfp}
                                    height='400px' width='400px' />
                                <hr />
                            </div>
                        </header>
                    </div>
                    <div>
                        <Container>
                            <Row>
                                <Col>
                                    <Card className="justify-content-start">
                                        <Card.Header>Attributes:</Card.Header>
                                        <Card.Body>
                                            <ListGroup variant="flush">
                                                <ListGroup.Item>State: {this.state.user.state}</ListGroup.Item>
                                                <ListGroup.Item>City: {this.state.user.city}</ListGroup.Item>
                                                <ListGroup.Item>Age: {this.state.user.age}</ListGroup.Item>
                                                <ListGroup.Item>Bio: {this.state.user.bio}</ListGroup.Item>
                                            </ListGroup>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <div>
                        {this.renderPosts()}
                    </div>
                </section>

        );
    }

    render() {
        return this.renderUser();

    }
}

export default UserModel;
