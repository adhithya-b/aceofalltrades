import pymongo

client = pymongo.MongoClient(open("connstring.txt", 'r').read())
db = client["aoat"]
usersCol = db['users']
postsCol = db['posts']

res = postsCol.find()

for x in res:
    if "user_id" not in x:
        postsCol.update_one({"_id":x['_id']}, {'$set': {'user_id':0}})