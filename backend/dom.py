class User:
    user_id - int
    name - string
    bio - string
    skills - array string
    age - int
    city - string
    state - string
    posts - array of post_id
    following - array of user_id
    pfp - b64 encoded image
 
class Post:
    post_id - int (auto generated)
    user_id - int 
    title - string
    description - string
    price - int
    tags - array of strings
    pictures - array of b64 encoded images

class Tags:
    tags - list of strings

Painting
Woodworking
Metalworking
Music
Video Editing
Photography
Photo Editing
Jewelry
Graphic Design
Sculpting
