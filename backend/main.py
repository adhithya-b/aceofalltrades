import json
import pymongo
from flask import Flask
from flask import request, abort
from flask_pymongo import PyMongo
from werkzeug.datastructures import ImmutableMultiDict
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.config["MONGO_URI"] = open("connstring.txt", 'r').read() #TODO change URI
print(open("connstring.txt", 'r').read())
mongo = PyMongo(app)


client = pymongo.MongoClient(open("connstring.txt", 'r').read())
db = client["aoat"]

@app.route("/api", methods=['GET'])
def test():
	testObj = {}
	testObj['database-names'] = client.list_database_names()
	testObj['collection-names'] = db.list_collection_names()
	l = []
	for x in db['users'].find():
		l.append(x)
	testObj['user'] = l
	return json.dumps(testObj)

# --- TAGS ---
@app.route("/api/tags", methods=['GET'])
def getTags():
	tagObj = db['tags'].find_one()
	return json.dumps( tagObj['tags'] )

# --- USER ---
@app.route("/api/user/<int:id>", methods=['GET'])
def getUser(id):
	return json.dumps( db['users'].find_one(id) )

@app.route("/api/user", methods=['POST'])
def postUser():
	formData = request.form.to_dict(flat=True)
	user = {
	    name : formData['name'],
	    bio : formData['bio'],
	    skills : [], #array string
	    age : formData['age'],
	    city : formData['city'],
	    state : formData['state'],
	    posts : [],
	    following : [],
	    pfp: formData['pfp'] # - b64 encoded image
	}
	db['users'].insert_one(user)
	return json.dumps(user)

@app.route("/api/user/follow", methods=['POST'])
def userFollow():
	follower = request.args["follower"]
	if follower == None:
		abort(400)
	else:
		follower = int(follower)

	followee = request.args["followee"]
	if followee == None:
		abort(400)
	else:
		followee = int(followee)

	proj = {'following':1}
	userFollowing = db['users'].find_one(follower,proj)
	following = []

	if userFollowing != None:
		following = userFollowing['following']

	if followee not in following:	
		following.append(followee)

	res = db['users'].update_one({'_id':follower}, { '$set': { 'following':following} })

	return json.dumps({'result':'success!'})


# --- POST ---
@app.route("/api/post/<int:id>", methods=['GET'])
def getPost(id):
	post = db['posts'].find_one(id)
	proj = { 
			    'name'      : 1,
			    'city'      : 1,
			    'state'     : 1,
			    'pfp'       : 1
			}
	post['user'] = db['users'].find_one(post['user_id'],proj)
	return json.dumps( post )


@app.route("/api/posts", methods=['GET'])
def getPosts():
	# Defaults
	skip = 0
	limit = 10

	limitArg = request.args.get("limit")
	if limitArg != None:
		limit = int(limitArg)

	pageArg = request.args.get("page") #page num
	if pageArg != None:
		skip = int(pageArg) * limit

	results = db['posts'].find(skip=skip, limit=limit)

	res = []
	for r in results:
		proj = { 
				    'name'      : 1,
				    'city'      : 1,
				    'state'     : 1,
				    'pfp'       : 1
				}
		r['user'] = db['users'].find_one(r['user_id'],proj)
		res.append(r)

	return json.dumps(res)

@app.route("/api/post", methods=['POST'])
def makePost():
	formData = request.form.to_dict(flat=False)
	post = {
	    'user_id' : formData['user_id'][0],			# int
	    'title' : formData['title'][0], 			# string
	    'description' : formData['description'][0],	# string
	    'forSale' : formData['forSale'][0], 		# bool
	    'price' : formData['price'][0], 			# int
	    'tags' : formData['tags'], 					# array of strings
	    'pictures' : formData['pictures']			# array of b64 encoded images
	}
	# db['users'].insert_one(formData)
	res = db['posts'].insert_one(post)
	u = db['users'].find({"_id" : formData['user_id'][0]})[0]
	u['posts'].append(res.inserted_id)
	db['users'].update_one(u["_id"], {"set": {'posts': u['posts']}})
	return json.dumps(post)
